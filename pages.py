from PyQt5.QtWidgets import *
import PyQt5.QtCore as QtCore
import thunderbird
import torbrowser


class BasicPage(QWizardPage):
    """
    Page with default settings for other pages to inherit
    """

    def __init__(self):
        super().__init__()
        self.defaultHeaderCoordinates = QtCore.QRect(180, 30, 280, 51)
        self.setStyleSheet(open("styles/style.qss", "r").read())


class FirstGeneralPage(BasicPage):
    """
    First page of the wizard
    """

    def __init__(self):
        super().__init__()
        self.page_definition()

    def page_definition(self):
        self.setObjectName("Introduction")
        text_browser_1_1 = QTextBrowser(self)
        text_browser_1_1.setGeometry(QtCore.QRect(70, 100, 480, 130))
        text_browser_1_1.setObjectName("Explanation Introduction")
        text_browser_1_1.textCursor().insertHtml(
            "<p>This is installation wizard for the Project Pandora. Next steps will navigate you through the "
            "installation process.</p>"
            ""
            "<p>Project Pandora is the installation software for the administrators or users to make configuration of "
            "activists tools simple and fast. Right now it installs Pidgin, Veracrypt, Thunderbird (with Tor), "
            "Torbrowser.</p>")
        text_browser_1_2 = QTextBrowser(self)
        text_browser_1_2.setGeometry(self.defaultHeaderCoordinates)
        text_browser_1_2.setObjectName("Heading Introduction")
        text_browser_1_2.setHtml("<p class='main-title' style='text-align: center'>Pandora Project</p>")
        text_browser_1_2.setProperty("class", "main-title")


class VeracryptInformationPage(BasicPage):
    """
    Veracrypt information Page
    """

    def __init__(self):
        super().__init__()
        self.veracrypt_information()

    def veracrypt_information(self):
        """First page of Veracrypt installation process"""
        self.setObjectName("Veracrypt Options")

        text_browser_1_1 = QTextBrowser(self)
        text_browser_1_1.setGeometry(self.defaultHeaderCoordinates)
        text_browser_1_1.setObjectName("Title")
        text_browser_1_1.setHtml("<p style='text-align: center'>Veracrypt</>")
        text_browser_1_1.setProperty("class", "main-title")

        text_browser_1_2 = QTextBrowser(self)
        text_browser_1_2.setGeometry(QtCore.QRect(70, 100, 480, 251))
        text_browser_1_2.setObjectName("Description Text")
        text_browser_1_2.setHtml("""
            <p>Veracrypt installation and volume creation. This part is essential for other installation steps: 
            without veracrypt volume you won't be able to move data from your thunderbird and pidgin profiles 
            into encrypted container. Torbrowser won't be placed into veracrypt container as well.</p>

            </ul>
        """)
        widget = QWidget(self)
        widget.setGeometry(QtCore.QRect(90, 140, 481, 83))
        vertical_layout_2 = QVBoxLayout(widget)
        vertical_layout_2.setObjectName("vertical_layout_2")
        self.veracrypt_install_checkbox = QCheckBox(widget)
        self.veracrypt_install_checkbox.setObjectName("self.veracrypt_default_checkbox")
        self.veracrypt_install_checkbox.setText("Install Veracrypt")
        self.veracrypt_install_checkbox.setChecked(True)
        vertical_layout_2.addWidget(self.veracrypt_install_checkbox)


class JabberInformationPage(BasicPage):
    def __init__(self):
        super().__init__()
        self.jabber_information_page()

    def jabber_information_page(self):
        self.setObjectName("Jabber Information Page")
        self.text_browser = QTextBrowser(self)
        self.text_browser.setGeometry(self.defaultHeaderCoordinates)
        self.text_browser.setObjectName("Header")
        self.text_browser.setHtml("<p class='main-title' style='text-align: center'>Jabber</p>")
        self.text_browser.setProperty("class", "main-title")
        self.text_browser_2 = QTextBrowser(self)
        self.text_browser_2.setGeometry(QtCore.QRect(70, 100, 480, 130))
        self.text_browser_2.setHtml("""
            <p>Moving to installation and configuration of jabber on your system. Next steps will guide you through 
            registration process of your new jabber account at https://jabber.systemli.org with installation of pidgin
            with OTR support on your system. Pidging profile in this steps will be moved to your Veracrypt container for
            extra security (that means that you should start your pidgin only after mounting your VC container.</p>
        """)


class PidginInstallPage(BasicPage):
    def __init__(self):
        super().__init__()
        self.pidgin_install_page()

    def pidgin_install_page(self):
        """
        Installation page of Pidgin client
        """

        self.setObjectName("Pidgin Client")
        self.verticalLayoutWidget = QWidget(self)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(90, 280, 481, 83))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("Vertical Layout")
        self.checkBox_1 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_1.setObjectName("Install Pidgin")
        self.checkBox_1.setText("Install Pidgin with OTR")
        self.checkBox_1.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_1)
        self.checkBox_2 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_2.setObjectName("Pidgin2VC")
        self.checkBox_2.setText("Move Pidgin profile to VC container")
        self.checkBox_2.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_2)
        self.checkBox = QCheckBox(self.verticalLayoutWidget)
        self.checkBox.setObjectName("Default Config")
        self.verticalLayout.addWidget(self.checkBox)
        self.checkBox.setText("Use default config")
        self.checkBox.setChecked(True)
        self.textBrowser_1 = QTextBrowser(self)
        self.textBrowser_1.setGeometry(self.defaultHeaderCoordinates)
        self.textBrowser_1.setObjectName("Header")
        self.textBrowser_1.setHtml("<p class='main-title' style='text-align: center'>Pidgin</p>")
        self.textBrowser_1.setProperty("class", "main-title")
        self.textBrowser_2 = QTextBrowser(self)
        self.textBrowser_2.setGeometry(QtCore.QRect(70, 100, 480, 130))
        self.textBrowser_2.setObjectName("pidgin information")
        self.textBrowser_2.setHtml("""
            <p>Installing pidgin on your computer with OTR support. If selected your pidgin profile will be copied to 
            VC container. After that it will be available only if you mount VC container correctly in the same slot.</p>

            <p>Configuration file is also prepared. It includes default ON of the OTR plugin. It disables all logging 
            in your pidgin and configures the application to use TOR network by default for connection.</p>
        """)


class ThunderbirdInformationPage(BasicPage):
    def __init__(self):
        super().__init__()
        self.thunderbird_information_page()

    def thunderbird_information_page(self):
        self.setObjectName("Thunderbird Information Page")
        self.text_browser = QTextBrowser(self)
        self.text_browser.setGeometry(self.defaultHeaderCoordinates)
        self.text_browser.setObjectName("Header")
        self.text_browser.setHtml("<p class='main-title' style='text-align: center'>Thunderbird</p>")
        self.text_browser.setProperty("class", "main-title")
        self.text_browser_2 = QTextBrowser(self)
        self.text_browser_2.setGeometry(QtCore.QRect(70, 100, 480, 130))
        self.text_browser_2.setHtml("""
            <p>This steps will install thunderbird with enigmail, that will help you use your emails 
            with PGP encryption. Apart from that it will move your thunderbird profile to previously created 
            VC container making it impossible to use encrypted emailing without mounting encrypted volume
            on your system. Unfortunately with new version of thunderbird Torbirdy is outdated. Configure your proxy
             setting to use .onion resources</p>
        """)
        self.setObjectName("Thunderbird Install")
        self.verticalLayoutWidget = QWidget(self)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(90, 280, 481, 83))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("Vertical Layout")
        self.checkBox_1 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_1.setObjectName("Install Thunderbird")
        self.checkBox_1.setText("Install Thunderbird with Enigmail")
        self.checkBox_1.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_1)
        self.checkBox_2 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_2.setObjectName("Thunderbird2VC")
        self.checkBox_2.setText("Move Thunderbird profile to VC container")
        self.checkBox_2.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_2)
        self.checkBox_3 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_3.setObjectName("GPG generation")
        self.verticalLayout.addWidget(self.checkBox_3)
        self.checkBox_3.setText("Generate pgp key for email")
        self.checkBox_3.setChecked(True)

    def thunderbird_actions(self):
        """
        Installing Thunderbird
        Installing Enigmail
        Creating profile in VC container
        Installing TorBirdy for the profile
        """
        tb = thunderbird.ThunderbirdInstaller()
        if self.checkBox_1.isChecked():
            tb.thunderbird_basic_install()
            if self.checkBox_2.isChecked():
                tb.thunderbird_profile_ini()
                tb.thunderbird_profile_create_vc()
            if self.checkBox_3.isChecked():
                tb.gpg_generate_key()


class TorbrowserInstallPage(BasicPage):
    def __init__(self):
        super().__init__()
        self.install_page()

    def install_page(self):
        self.setObjectName("Torbrowser install page")
        self.text_browser = QTextBrowser(self)
        self.text_browser.setGeometry(self.defaultHeaderCoordinates)
        self.text_browser.setObjectName("Header")
        self.text_browser.setHtml("<p class='main-title' style='text-align: center'>Torbrowser</p>")
        self.text_browser.setProperty("class", "main-title")
        self.text_browser_2 = QTextBrowser(self)
        self.text_browser_2.setGeometry(QtCore.QRect(70, 100, 480, 130))
        self.text_browser_2.setHtml("""
            <p>This steps will install Torbrowser on your computer with possibility to move torbrowser profile
            into your veracrypt container. We recommend you do that. Apart from that binary file will be created that 
            will give you possibility to start torbrowser as any other application. </P
        """)
        self.setObjectName("Torbrowser install")
        self.verticalLayoutWidget = QWidget(self)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(90, 280, 481, 83))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("Vertical Layout")
        self.checkBox_1 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_1.setObjectName("Install Torbrowser")
        self.checkBox_1.setText("Install Torbrowser")
        self.checkBox_1.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_1)
        self.checkBox_2 = QCheckBox(self.verticalLayoutWidget)
        self.checkBox_2.setObjectName("Torbrowser2VC")
        self.checkBox_2.setText("Move Torbrowser profile to VC container")
        self.checkBox_2.setChecked(True)
        self.verticalLayout.addWidget(self.checkBox_2)

    def torbrowser_install_actions(self):
        """
        Connecting checkboxes to actions
        """
        tor = torbrowser.TorBrowser()
        if self.checkBox_1.isChecked():
            tor.install_torbrowser()
            if self.checkBox_2.isChecked():
                tor.move_to_veracrypt()


class LastGeneralPage(BasicPage):
    """
    Last page of the wizard
    """

    def __init__(self):
        super().__init__()
        self.page_definition()

    def page_definition(self):
        self.setObjectName("EndOfInstall")
        self.text_browser_1_1 = QTextBrowser(self)
        self.text_browser_1_1.setGeometry(QtCore.QRect(70, 100, 480, 130))
        self.text_browser_1_1.setObjectName("Installation is Over")
        self.text_browser_1_1.textCursor().insertHtml(
            "<p>Installation process is over. If everything went right you have a small setup for activist work."
            "This installation is minimum setup. If you ware interested in more security features please"
            "check online guides on such sites like aktivix, riseup and systemli</p>"
            "")
        self.text_browser_1_2 = QTextBrowser(self)
        self.text_browser_1_2.setGeometry(self.defaultHeaderCoordinates)
        self.text_browser_1_2.setObjectName("Heading Ending")
        self.text_browser_1_2.setHtml("<p class='main-title' style='text-align: center'>Pandora Project</p>")
        self.text_browser_1_2.setProperty("class", "main-title")
