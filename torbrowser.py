from veracrypt import PackageManagement
import logging
import gnupg
import os

TORBROWSERVERSION = "9.0.5"


class TorBrowser(PackageManagement):
    """
    Management of Tor Browser installation
    """
    logging.getLogger().setLevel(logging.INFO)

    def torbrowser_verification(self, package="/tmp/torbrowser.tar.bz2",
                                signature="/tmp/torbrowser.tar.bz2.asc"):
        """
        Verifying gpg signature of the browser package
        :param package: path to package to check
        :param signature: signature file to check the package against
        :return True or False
        """
        gpg = gnupg.GPG(gnupghome="{}/.gnupg/".format(os.path.expanduser("~")))  # defaults to home folder of the user
        logging.info("Downloading Torbrowser development keys for verification")
        key = open("Torbrowser/public.asc", "r")
        gpg.import_keys(key.read())
        return self.verify_package(package, signature)

    def install_torbrowser(self):
        """
        Download and verify torbrowser package
        """
        logging.info("Downloading Tor Browser")
        self.download("https://www.torproject.org/dist/torbrowser/{version}/"
                      "tor-browser-linux64-{version}_en-US.tar.xz"
                      .format(version=TORBROWSERVERSION),
                      "torbrowser.tar.bz2")
        self.download("https://www.torproject.org/dist/torbrowser/{version}/"
                      "tor-browser-linux64-{version}_en-US.tar.xz.asc"
                      .format(version=TORBROWSERVERSION),
                      "torbrowser.tar.bz2.asc")
        if self.torbrowser_verification():
            logging.info("Verification passed, installing torbrowser")
            os.system("tar xf /tmp/torbrowser.tar.bz2 -C {}/".format(os.path.expanduser("~")))
            logging.info("Cleaning up")
            os.system("rm /tmp/torbrowser.tar.bz2 /tmp/torbrowser.tar.bz2.asc")
        else:
            logging.error("Signature verification failed, you might be under attack")
            os.system("rm /tmp/torbrowser.tar.bz2 /tmp/torbrowser.tar.bz2.asc")

    @staticmethod
    def move_to_veracrypt():
        """Moving torbrowser profile to veracrypt container"""
        os.system("mv {}/tor-browser_en-US /media/veracrypt1/".format(os.path.expanduser("~")))
