import os
import logging

class ThunderbirdInstaller(object):
    """
    Installation of thunderbird and enigmail.
    Moving thunderbird profile into veracrypt container
    """
    def __init__(self):
        self.vc_container = "/media/veracrypt1/thunderbird"
        self.vc_profile = "Thunderbird.vc"
        self.terminals = ["gnome-terminal", "lxterminal", "mate-terminal", "xfce4-terminal", "konsole"]

    def return_terminal(self):
        """
        Returns type of installed terminal depending on distribution
        :return:
        """
        for term in self.terminals:
            if os.system("{} --version".format(term)) == 0:
                return term

    @staticmethod
    def thunderbird_basic_install():
        """
        Installing packages
        """
        logging.info("Installing thunderbird and enigmail")
        os.system("pkexec bash -c 'apt install -y thunderbird enigmail'")

    @staticmethod
    def thunderbird_profile_ini():
        """
        Moving thunderbird INI profile
        """
        logging.info("Moving profiles.ini to thunderbird folder")
        os.system('cp ./thunderbird/profiles.ini ~/.thunderbird/')  # moving our own profiles.ini to .thunderbird


    def gpg_generate_key(self):
        """
        Generating key for the used if specified
        """
        logging.info("Generating pgp key for secure communication")
        os.system("{} -e \"bash -c 'gpg --full-gen-key'\"".format(self.return_terminal()))

    def thunderbird_profile_create_vc(self):
        """
        Creating profile in VC container
        """
        logging.info("Creating thunderbird profile in veracrypt folder")
        create_profile = "thunderbird -CreateProfile \"{0} {1}\"".format(self.vc_profile, self.vc_container)
        os.system(create_profile)

