# Should be implemented

* Veracrypt Install
    * (+) Veracrypt Volume Creation
    * (+) Veracrypt Download 
    * (+) Veracrypt Download Verification
    * (+) Veracrypt verification process is not working
    * (-) Use the new veracrypt installer 
* Thunderbird + Enigmail
    * (+) Thunderbird Installation
    * (+) Thunderbird profilemigration
    * (+) Thunderbird enigmail activation
    * (+) Implement GUI
    * (+) GPG generate key
* Pidgin
    * (+) Pidgin-OTR installation
    * (+) Pidgin profile registration
    * (+) Pigdin profile to veracrypt migration
    * (+) Pidgin default configurations with OTR enabled
    * (+) UI Implementation
* Torbrowser install
    * (+) Download tor
    * (+) Verify tor browser bundle
    * (+) TorBrowser on Veracrypt
* Should include log part in all the installation windows
* Problem with password if no terminal opened
* Multithreading to prevent the hanging condition of the application
* (+) One file solution with static files added to the folder 
* (+) Create How-To