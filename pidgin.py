import os
import logging


class PidginInstallation(object):
    """
    Installation of Pidgin on the computer with migration of pidging profile to secure location
    """

    @staticmethod
    def pidgin_otr_install():
        """
        Installing pidgin and OTR support on it
        """
        logging.info("Installing pidgin and OTR support")
        os.system("pkexec bash -c 'apt install -y pidgin pidgin-otr'")

    @staticmethod
    def pidgin_profile_in_vc():
        """
        Move pidgin profile into VC container if mounted
        """
        logging.info("Moving profiles to veracrypt folder")
        os.system("mkdir /media/veracrypt1/.purple")
        os.system("ln -s /media/veracrypt1/.purple ~/")

    @staticmethod
    def pidgin_default_config():
        """
        Moving default config to the pidgin profile folder
        Includes tor proxy, disabled history, OTR plugin activation
        """
        logging.info("Copying default profile to pidgin folder")
        os.system("cp ./pidgin/prefs.xml ~/.purple/")
