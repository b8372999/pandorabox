import PyQt5.QtGui as QtGui
import sys
import veracrypt as vc
import pidgin
from pages import *


class MainWindow(QWizard):
    def __init__(self):
        super().__init__()
        self.title = "Installation Wizard"
        self.left = 200
        self.top = 200
        self.width = 640
        self.height = 480
        self.defaultHeaderCoordinates = QtCore.QRect(180, 30, 280, 51)
        self.vc_install = VeracryptInformationPage()
        self.pidgin_install = PidginInstallPage()
        self.thunderbird_install = ThunderbirdInformationPage()
        self.torbrowser_install = TorbrowserInstallPage()
        self.init_ui()

    def init_ui(self):
        self.setWizardStyle(QWizard.AeroStyle)
        self.setObjectName("PB Wizard")
        self.resize(self.width, self.height)
        self.setStyleSheet(open("styles/style.qss", "r").read())
        self.addPage(FirstGeneralPage())
        self.button(QWizard.NextButton).clicked.connect(self.veracrypt_install_button)
        self.addPage(self.vc_install)
        self.vc_install.veracrypt_install_checkbox.clicked.connect(self.veracrypt_skip)
        self.addPage(JabberInformationPage())
        self.pidgin_install.checkBox_1.clicked.connect(self.pidgin_skip)
        self.button(QWizard.NextButton).clicked.connect(self.pidgin_install_button)
        self.addPage(self.pidgin_install)
        self.thunderbird_install.checkBox_1.clicked.connect(self.thunderbird_skip)
        self.button(QWizard.NextButton).clicked.connect(self.thunderbird_install_button)
        self.addPage(self.thunderbird_install)
        self.button(QWizard.NextButton).clicked.connect(self.torbrowser_install_button)
        self.torbrowser_install.checkBox_1.clicked.connect(self.torbrowser_skip)
        self.addPage(self.torbrowser_install)
        self.addPage(LastGeneralPage())
        self.show()

    def veracrypt_install_button(self):
        """Changing name of the "Next" button and binding it to Veracrypt installation process"""
        if self.currentId() == 1:
            self.button(self.NextButton).setText("Install")
        if self.currentId() == 2 and self.vc_install.veracrypt_install_checkbox.isChecked():
            crypt = vc.Veracrypt()
            crypt.full_install()
        if not self.vc_install.veracrypt_install_checkbox:
            print("Veracrypt not installed")

    def pidgin_install_button(self):
        if self.currentId() == 4:
            pidgin_install = pidgin.PidginInstallation()
            if self.pidgin_install.checkBox_1.isChecked():
                QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
                pidgin_install.pidgin_otr_install()
                if self.pidgin_install.checkBox_2.isChecked():
                    pidgin_install.pidgin_profile_in_vc()
                if self.pidgin_install.checkBox.isChecked():
                    pidgin_install.pidgin_default_config()
            QtGui.QGuiApplication.restoreOverrideCursor()

    def thunderbird_install_button(self):
        if self.currentId() == 5:
            QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.thunderbird_install.thunderbird_actions()
            QtGui.QGuiApplication.restoreOverrideCursor()

    def torbrowser_install_button(self):
        if self.currentId() == 6:
            QtGui.QGuiApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
            self.torbrowser_install.torbrowser_install_actions()
            QtGui.QGuiApplication.restoreOverrideCursor()

    # Adjusting button value in case users decided to skip certain package
    def veracrypt_skip(self):
        if not self.vc_install.veracrypt_install_checkbox.isChecked():
            self.button(self.NextButton).setEnabled(True)
            self.button(self.NextButton).setText("Skip")
        else:
            self.button(self.NextButton).setText("Install")

    def pidgin_skip(self):
        if self.pidgin_install.checkBox_1.isChecked():
            self.button(self.NextButton).setText("Install")
        else:
            self.button(self.NextButton).setText("Skip")

    def thunderbird_skip(self):
        if self.thunderbird_install.checkBox_1.isChecked():
            self.button(self.NextButton).setText("Install")
        else:
            self.button(self.NextButton).setText("Skip")

    def torbrowser_skip(self):
        if self.torbrowser_install.checkBox_1.isChecked():
            self.button(self.NextButton).setText("Install")
        else:
            self.button(self.NextButton).setText("Skip")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
