import requests
import os
import gnupg
import logging
import distro

VERACRYPT_VERSION = "1.24"

DISTROS = {
    "10": "Debian-10",
    "19.10": "Ubuntu-19.10",
    "18.04": "Ubuntu-18.04",
    "20.04": "Ubuntu-20.04",
}

class PackageManagement(object):
    """
    Class for package management
    """
    @staticmethod
    def download(url, name):
        """
        downloading package from URL
        :param url: url of the package
        :param name: name to save the package
        """
        try:
            download_package = requests.get(url)
            if download_package.status_code == 200:
                with open('/tmp/{0}'.format(name), 'wb') as download_file:
                    download_file.write(download_package.content)
        except Exception as e:
            logging.error("Package couldn't be downloaded")
            print(e)


    def verify_package(self, package, signature):
        """
        Verify package GPG signature
        :param package: path to package to verify
        :param signature: signature file to verify against
        :return: returns True upon correct verification or False on failure
        """
        gpg = gnupg.GPG(gnupghome="{}/.gnupg/".format(os.path.expanduser("~")))
        signature = open(signature, "rb")
        verified = gpg.verify_file(signature, package)
        return verified

class Veracrypt(PackageManagement):
    """
    Class for interaction with Veracrypt
    """

    def os_version(self):
        """
        :return: returns current distribution and it's version
        """
        return DISTROS[distro.linux_distribution()[1]]

    def verify_veracrypt(self, package, signature):
        """
        Verify package PGP signature
        :param package: package path
        :param signature: signature of the package
        :returns True or False
        """
        veracrypt_pub_fingerprint = "5069A233D55A0EEB174A5FC3821ACD02680D16DE"
        gpg = gnupg.GPG(gnupghome="{}/.gnupg/".format(os.path.expanduser("~")))  # defaults to home folder of the user
        self.download("https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc", "veracrypt.asc")
        if gpg.scan_keys("/tmp/veracrypt.asc").fingerprints[0] == veracrypt_pub_fingerprint:
            key = open("/tmp/veracrypt.asc", "r")
            gpg.import_keys(key.read())
            return self.verify_package(package, signature)
        else:  # if the key's fingerprint doesn't match than False is returned
            logging.error("Fingerprint of the downloaded key is incorrect")
            return False

    def download_veracrypt(self):
        """
        Download and verify Veracrypt package
        """
        logging.info("starting veracrypt download")
        version = self.os_version()
        self.download("https://launchpad.net/veracrypt/trunk/{0}/+download/veracrypt-{0}-{1}-amd64.deb"
                 .format(VERACRYPT_VERSION, version), "veracrypt.deb")
        self.download("https://launchpad.net/veracrypt/trunk/{0}/+download/veracrypt-{0}-{1}-amd64.deb.sig"
                 .format(VERACRYPT_VERSION, version), "veracrypt.deb.sig")
        logging.info("Veracrypt have been downloaded")
        logging.info("Verifying veracrypt package")
        return self.verify_veracrypt("/tmp/veracrypt.deb", "/tmp/veracrypt.deb.sig")

    @staticmethod
    def install_veracrypt():
        """
        Unzip and install veracrypt with GUI
        """
        os.system("pkexec bash -c 'apt install -y libwxgtk3.0-gtk3-0v5 && dpkg -i /tmp/veracrypt.deb'")
        os.system("rm /tmp/veracrypt.*")


    def full_install(self):
        """
        Full installation of the veracrypt with volume creation
        :param default: Use default option?
        :param path: Path to the veracrypt volume
        :param password: Password for veracrypt volume
        :return:
        """
        if self.download_veracrypt():
            self.install_veracrypt()
            os.system("veracrypt")
        else:
            os.remove("/tmp/veracrypt.deb.bz2")
            os.remove("/tmp/veracrypt.deb.sig")
